class User < ApplicationRecord
    has_many :bookings
    has_many :books, through: :bookings
end
