class CreateUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.integer :dni
      t.string :phone
      t.string :rol

      t.timestamps
    end
  end
end
