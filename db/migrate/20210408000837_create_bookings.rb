class CreateBookings < ActiveRecord::Migration[6.1]
  def change
    create_table :bookings do |t|
      t.integer :book_id
      t.integer :user_dni
      t.datetime :start_booking
      t.datetime :end_time
      t.string :state

      t.timestamps
    end
  end
end
